import React, { useState, useEffect } from 'react';
import Map from './Map';
import Form from './Form';
import Toaster from '../common/Toaster';
import { getGeocode } from '../../service/api/geocode';
import { createJob } from '../../service/api/jobs';
import sleep from '../helpers/sleep';
import useDebounce from '../helpers/useDebounce';

const JobsCreator = () => {
  const [pickUpInfo, setPickUpInfo] = useState({});
  const [dropOffInfo, setDropOffInfo] = useState({});
  const [pickUpAddress, setPickUpAddress] = useState('');
  const [dropOffAddress, setDropOffAddress] = useState('');
  const [showToaster, setShowToaster] = useState(false);
  const [validationButton, setValidationButton] = useState({ wording: 'Create job', disable: true });
  const debouncedPickUpAddress = useDebounce(pickUpAddress, 500);
  const debouncedDropOffAddress = useDebounce(dropOffAddress, 500);

  // Enable/disable the submit button
  useEffect(() => {
    if (pickUpInfo.valid && dropOffInfo.valid) {
      setValidationButton({ wording: 'Create job', disable: false });
    } else {
      setValidationButton({ wording: 'Create job', disable: true });
    }
  }, [pickUpInfo, dropOffInfo]);

  // geocode the pick up address when the user stop typing after 0.5s
  useEffect(() => {
    if (debouncedPickUpAddress && debouncedPickUpAddress !== 'An invalid pick up address') {
      getGeocode(debouncedPickUpAddress)
        .then((response) => {
          setPickUpInfo({ valid: true, lat: response.latitude, lng: response.longitude });
        })
        .catch((error) => {
          setPickUpInfo({ valid: false });
        });
    }
  }, [debouncedPickUpAddress]);

  // geocode the drop off address when the user stop typing after 0.5s
  useEffect(() => {
    if (debouncedDropOffAddress && debouncedDropOffAddress !== 'An invalid drop off address') {
      getGeocode(debouncedDropOffAddress)
        .then((response) => {
          setDropOffInfo({ valid: true, lat: response.latitude, lng: response.longitude });
        })
        .catch((error) => {
          setDropOffInfo({ valid: false });
        });
    }
  }, [debouncedDropOffAddress]);

  // Hide the toaster after 5s
  useEffect(() => {
    if (showToaster) hideToaster(5000);
  }, [showToaster]);

  // geocode the pick up address on blur
  const OnBlurPickUpAddress = () => {
    if (pickUpAddress !== '' && !pickUpInfo.valid) {
      getGeocode(pickUpAddress)
        .then((response) => {
          setPickUpInfo({ valid: true, lat: response.latitude, lng: response.longitude });
        })
        .catch((error) => {
          setPickUpInfo({ valid: false });
          setPickUpAddress('An invalid pick up address');
        });
    }
  };

  // geocode the drop off address on blur
  const OnBlurDropOffAddress = () => {
    console.log(dropOffInfo);
    if (dropOffAddress !== '' && !dropOffInfo.valid) {
      getGeocode(dropOffAddress)
        .then((response) => {
          setDropOffInfo({ valid: true, lat: response.latitude, lng: response.longitude });
        })
        .catch((error) => {
          setDropOffInfo({ valid: false });
          setDropOffAddress('An invalid drop off address');
        });
    }
  };

  const onSubmit = () => {
    setValidationButton({ wording: 'Creating...', disable: true });

    createJob(pickUpAddress, dropOffAddress)
      .then((response) => {
        setPickUpAddress('');
        setDropOffAddress('');
        setPickUpInfo({ valid: false });
        setDropOffInfo({ valid: false });
        setValidationButton({ wording: 'Create job', disable: true });
        setShowToaster(true);
      })
      .catch((error) => {
        setValidationButton({ wording: 'Error during the job creation', disable: true });
      });
  };

  const hideToaster = async (time = 0) => {
    await sleep(time);
    setShowToaster(false);
  };

  return (
    <>
      <Map dropOff={dropOffInfo} pickUp={pickUpInfo} />
      <Form
        onSubmit={onSubmit}
        validationButton={validationButton}
        pickUpAddress={pickUpAddress}
        pickUpInfo={pickUpInfo}
        setPickUpAddress={setPickUpAddress}
        OnBlurPickUpAddress={OnBlurPickUpAddress}
        dropOffAddress={dropOffAddress}
        dropOffInfo={dropOffInfo}
        setDropOffAddress={setDropOffAddress}
        OnBlurDropOffAddress={OnBlurDropOffAddress}
      />
      {showToaster && (
        <div onClick={hideToaster}>
          <Toaster />
        </div>
      )}
    </>
  );
};

export default JobsCreator;
