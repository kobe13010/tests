import React from 'react';
import PropTypes from 'prop-types';
import { GoogleMap, LoadScript, Marker } from '@react-google-maps/api';

const Map = ({ pickUp, dropOff }) => {
  return (
    <LoadScript id="script-loader" googleMapsApiKey="">
      <GoogleMap
        id="jobs-map"
        mapContainerStyle={{
          height: '800px',
          width: '100%',
        }}
        options={{ fullscreenControl: false }}
        zoom={13}
        center={{
          lat: 48.866667,
          lng: 2.333333,
        }}>
        {pickUp && pickUp.valid && (
          <Marker
            position={{
              lat: pickUp.lat,
              lng: pickUp.lng,
            }}
            icon="/assets/pickUpMarker.svg"
          />
        )}
        {dropOff && dropOff.valid && (
          <Marker
            position={{
              lat: dropOff.lat,
              lng: dropOff.lng,
            }}
            icon="/assets/dropOffMarker.svg"
          />
        )}
      </GoogleMap>
    </LoadScript>
  );
};

Map.propTypes = {
  pickUp: PropTypes.shape({
    valid: PropTypes.bool,
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
  dropOff: PropTypes.shape({
    valid: PropTypes.bool,
    lat: PropTypes.number,
    lng: PropTypes.number,
  }),
};

export default Map;
