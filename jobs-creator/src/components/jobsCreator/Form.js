import React from 'react';
import PropTypes from 'prop-types';

const Form = ({
  onSubmit,
  validationButton,
  pickUpAddress,
  pickUpInfo,
  setPickUpAddress,
  OnBlurPickUpAddress,
  dropOffAddress,
  dropOffInfo,
  setDropOffAddress,
  OnBlurDropOffAddress,
}) => (
  <form className="form-group mt-3 mx-auto">
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <span className="input-group-text">
          {pickUpAddress === '' && <img height="23" width="23" alt="pick up icon" src="/assets/pickUpBadgeBlank.svg" />}
          {pickUpInfo.valid && pickUpAddress !== '' && (
            <img height="23" width="23" alt="pick up icon" src="/assets/pickUpBadgePresent.svg" />
          )}
          {!pickUpInfo.valid && pickUpAddress !== '' && (
            <img height="23" width="23" alt="pick up icon" src="/assets/pickUpBadgeError.svg" />
          )}
        </span>
      </div>
      <input
        value={pickUpAddress}
        onChange={(e) => setPickUpAddress(e.target.value)}
        onBlur={OnBlurPickUpAddress}
        className="form-control"
        placeholder="Pick up address"
        type="text"
        name="PickUpAddress"
        required
      />
    </div>
    <div className="input-group mb-3">
      <div className="input-group-prepend">
        <span className="input-group-text">
          {dropOffAddress === '' && (
            <img height="23" width="23" alt="drop off icon" src="/assets/dropOffBadgeBlank.svg" />
          )}
          {dropOffInfo.valid && dropOffAddress !== '' && (
            <img height="23" width="23" alt="drop off  icon" src="/assets/dropOffBadgePresent.svg" />
          )}
          {!dropOffInfo.valid && dropOffAddress !== '' && (
            <img height="23" width="23" alt="drop off icon" src="/assets/dropOffBadgeError.svg" />
          )}
        </span>
      </div>
      <input
        value={dropOffAddress}
        onChange={(e) => setDropOffAddress(e.target.value)}
        onBlur={OnBlurDropOffAddress}
        className="form-control"
        placeholder="Drop off address"
        type="text"
        name="DropOffAddress"
        required
      />
    </div>
    <button
      className="btn btn-primary mr-3 btn-block"
      onClick={onSubmit}
      type="submit"
      disabled={validationButton.disable}>
      {validationButton.wording}
    </button>
  </form>
);

Form.propTypes = {
  onSubmit: PropTypes.func,
  validationButton: PropTypes.object,
  pickUpAddress: PropTypes.string,
  pickUpInfo: PropTypes.object,
  setPickUpAddress: PropTypes.func,
  OnBlurPickUpAddress: PropTypes.func,
  dropOffAddress: PropTypes.string,
  dropOffInfo: PropTypes.object,
  setDropOffAddress: PropTypes.func,
  OnBlurDropOffAddress: PropTypes.func,
};

export default Form;
