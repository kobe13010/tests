import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'https://stuart-frontend-challenge.now.sh/',
  timeout: 5000,
});

function post(url, data) {
  const requestConf = {
    method: 'post',
    url,
    data,
  };

  return axiosInstance(requestConf)
    .then((res) => res.data)
    .catch((err) => {
      console.log(err);
      throw err;
    });
}

export default {
  post,
};
