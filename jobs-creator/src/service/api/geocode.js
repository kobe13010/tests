import api from '../apiCaller';

export function getGeocode(data) {
  return api.post('geocode', { address: data });
}
