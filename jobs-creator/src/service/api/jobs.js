import api from '../apiCaller';

export function createJob(pickUp, dropOff) {
  return api.post('jobs', { pickup: pickUp, dropoff: dropOff });
}
