import React from 'react';
import JobsCreator from './components/jobsCreator/JobsCreator';

const App = () => (
  <div className="mx-auto">
    <header className="App-header mb-3 mt-3">
      <h1 className="text-center">Jobs Creator</h1>
    </header>
    <JobsCreator />
  </div>
);

export default App;
