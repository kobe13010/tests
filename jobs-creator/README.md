To be able to run the app locally you need to run these commands:

```
- yarn
- yarn start (run the local environment and watch files)

Open [http://localhost:3000](http://localhost:3000) to view the app in the browser.

```

## Other Available Scripts

### `yarn run format`

Runs the prettier formatting and the lint fixing in order to have consistent code base.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn analyse`

Analyzes JavaScript bundles using the source maps. This helps you understand where code bloat is coming from (you need to create the production build first).

### `yarn run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.
